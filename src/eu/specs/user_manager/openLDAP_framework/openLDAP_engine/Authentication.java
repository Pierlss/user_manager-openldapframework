package eu.specs.user_manager.openLDAP_framework.openLDAP_engine;

import javax.naming.NamingException;

import eu.specs.user_manager.openLDAP_framework.openLDAP_rest_client.User;

public class Authentication {
	public static Boolean loginReq(User u){
		
		Boolean auth=false;
		String username = u.getUsername();
	    String password = u.getPassword();
	    String role = u.getRole();
		Ldap ldap= new Ldap();
		
		try {
			if(ldap.validateLogin(username, password, role+"s")==true){
				System.out.println("[OpenLDAP Framework] Valid Authentication= user: "+username+", "
                		+ "password: "+password+", role: "+role);
				auth=true;
			}else{
				System.err.println("[OpenLDAP Framework] Invalid Authentication= user: "+username+", "
                		+ "password: "+password+", role: "+role);
                auth=false;
            }
		} catch (NamingException e) {
			auth=false;
			System.err.println("[OpenLDAP Framework] Invalid Authentication= user: "+username+", "
            		+ "password: "+password+", role: "+role);
			e.printStackTrace();
		}
		return auth;
	}
}
