package eu.specs.user_manager.openLDAP_framework.openLDAP_rest_client;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import eu.specs.user_manager.openLDAP_framework.openLDAP_engine.Authentication;
import eu.specs.user_manager.openLDAP_framework.openLDAP_engine.Signup;

@Controller
public class OpenLDAP_controller {
	//LOGIN: Riceve la tripla derivante dalla WebInterface e 
	// risponse con "OK" o "NOT OK" a seconda dell'esito
	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public String receiveReq(@RequestBody String body){
		//System.out.println("[OpenLDAP Framework] Login Request by: "+body);
		User u=StringToUser(body);
		System.out.println("[OpenLDAP Framework] Login Request received: "+u.getUsername()+" "+u.getPassword()+" "+u.getRole());
				
		//Richiama loginReq per attivare il meccanismo di Autenticazione ed
		//invia la risposta alla WebInterface (return OK o NOT OK)
		if(Authentication.loginReq(u)==true){
			return "OK";
		}else{
			return "NOT OK";
		}
	}
	
	//SIGNUP:Riceve la tripla derivante dalla WebInterface e 
	// risponde con "OK" o "NOT OK" a seconda dell'esito
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	@ResponseBody
	public String receiveReqSignup(@RequestBody String body){
		//System.out.println("[OpenLDAP Framework] Login Request by: "+body);
		User u=StringToUser(body);
		System.out.println("[OpenLDAP Framework] Signup Request received: "+u.getUsername()+" "+u.getPassword()+" "+u.getRole());
		
		//Richiama loginReq per attivare il meccanismo di Signup ed
		//invia la risposta alla WebInterface (return OK o NOT OK)
		if(Signup.signupReq(u)==true){
			return "OK";
		}else{
			return "NOT OK";
		}
	}
	
	
	//Riceve in ingresso la stringa del body e preleva i dati dello user
	private User StringToUser(String s){
		String u="",pwd="",role="";
		int count=1;
		while(s.charAt(count)!=','){
			u+=s.charAt(count);
			count++;
		}
		count++;
		while(s.charAt(count)!=','){
			pwd+=s.charAt(count);
			count++;
		}
		count++;
		while(s.charAt(count)!='}'){
			role+=s.charAt(count);
			count++;
		}
		
		return new User(u,pwd,role);
		
	}
}
